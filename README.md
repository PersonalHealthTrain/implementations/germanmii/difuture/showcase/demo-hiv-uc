# Demo HIV Train
- HIV can use different co-receptors for host cell entry (tropism)
- Useable co-receptors (CCR4, CXCR4) are defined by viral genome
- Sequencing yields patient-specific data sets (viral genomes)

The following demo train can also be used to test the station installation. The following `entrypoint.py`algorithm is
learning a binary classification problem for the following Use Case:
The 766 partial genome sequences data of subjects is distributed over three different FHIR Servers (IBM, Blaze and HAPI)
hosted at deNBI instances.

If you want to run this example with a self-deployed station, please 
request credentials for one of the desired FHIR server by us.

![hiv demo](images/demo-problem.png)

### Data
Data from 766 subjects ``sequences.txt`` are random distributed over three sites (LMU, TUM, UKT) was created which
are hosted on different FHIR servers.

#### FHIR-mapping
Based on the `sequence.txt` file different FHIR bundles were created. You can reproduce this step using the this [repository](https://gitlab.com/PersonalHealthTrain/implementations/germanmii/difuture/station/sandbox/fhir-station-mapping)


### Master image 
Based on the Python [slim dockerfile](https://gitlab.com/PersonalHealthTrain/implementations/germanmii/difuture/train-container-library/-/blob/demo/docker_files/Dockerfile_slim)
, a valid master image ``slim`` was created. It also contains the train operations for the [HIV Demo Train](https://gitlab.com/PersonalHealthTrain/implementations/germanmii/difuture/train-container-library/-/blob/master/train_lib/train/HIV_Train.py)
which is used for loading and saving of queries and results and functions of the train.

Master images are publicly available for validation.

### Train
The algorithm ``entrypoint.py`` requests over the train library the FHIR data at each station
and learns and validates a binary naive bayes classifier. The performance over all runs is plotted.
The submission process is described in the
[gitlab documentation](https://gitlab.com/PersonalHealthTrain/implementations/germanmii/difuture/documentation/-/blob/master/docs/user_interface.md)
of the user interface.

### Execution
Depending on the FHIR Servers and number of requested samples, the execution time varied:

You can request credentials of our [IBM](https://ibm.github.io/FHIR/), [Blaze](https://github.com/samply/blaze) or [HAPI](https://hapifhir.io) FHIR Servers:

TODO: Will follow after re-run.

[comment]: <> ( ![hiv runtome]&#40;images/demo-runtime.png&#41;)


### Results
TODO: Will follow after re-run.