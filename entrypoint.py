#!/usr/bin/env python
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
import asyncio
import numpy as np

from train_lib.fhir.FhirLoading import gen_search_query
from train_lib.train.HIV_Train import Train


def main():
    train = Train(model='model.pkl', results='analysis_results.pkl', query='query.json')

    loop = asyncio.get_event_loop()
    query_lst, output_list, media = train.load_queries()
    pat_df = loop.run_until_complete(gen_search_query(query_lst[0], output_list, "MolSeq"))
    del query_lst[0]
    query_file = {"query_lst": query_lst, "output_param_list": output_list, "media": "MolSeq"}
    train.save_queries(query_file)

    # Data Loading and class encoding
    x, y = train.load_sequences(pat_df, class_labels={
        'CCR5': 1,
        'CXCR4': 0
    })

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.10)
    classes = np.array([0, 1])

    results = train.load_results()
    print(results)

    station = str(len(results["analysis"]) + 1)
    if station == str(1):
        print("New Model is created")
        model = MultinomialNB(alpha=0.01)
        acc = 0
    else:
        print("Model exists")
        model = train.load_model()
        acc = model.score(x_test, y_test)

    model.partial_fit(x_train, y_train, classes=classes)
    new_acc = model.score(x_test, y_test)

    if acc == 0:
        acc = new_acc

    # Include hold out data in model before leaving station
    model.partial_fit(x_test, y_test, classes=classes)
    train.save_model(model)

    analysis = {'training_samples': len(x_train),
                'test_samples': len(x_test),
                'acc_local': acc,
                'acc_total': new_acc}
    results['analysis']['analysis_exec_' + str(len(results['analysis']) + 1)] = analysis

    print(results)
    train.plot_results(results['analysis'])
    train.save_results(results)


if __name__ == "__main__":
    main()

